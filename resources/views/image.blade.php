@foreach($images as $image)
    <tr>
        <td class="col-md-1">{{ $image->id }}</td>
        <td class="col-md-3"><img src="{{ Storage::url($image->path) }}" width="100%"></td>
        <td class="col-md-7">
            <form class="update_form" action="{{ route('images.update', ['id' => $image->id]) }}" method="POST" onsubmit="event.preventDefault()">
                {{ csrf_field() }}
                {{ method_field('POST') }}

                <input type="text" name="name" value="{{ $image->name }}" disabled style="width:100%;">
            </form>
        </td>
        <td class="col-md-1">
            <i class="edit glyphicon glyphicon-pencil"></i>
            <form class="destroy_form" action="{{ route('images.destroy', ['id' => $image->id]) }}" method="POST" onsubmit="event.preventDefault()">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
            </form>
            <i class="destroy glyphicon glyphicon-trash"></i>
        </td>
    </tr>
@endforeach