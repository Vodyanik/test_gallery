@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">You can select more than one photo</div>

                <div class="panel-body">
                    <form id="upload_form" action="{{ route('images.store') }}" method="POST" onsubmit="event.preventDefault()" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('POST') }}

                        <label for="upload">Select File</label>
                        <input id="upload" name="images[]" type="file" multiple>

                        <input type="submit" class="btn btn-success" value="Сохранить" style="width: 100%">
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">Photos</div>

                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Image</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($images as $image)
                                <tr>
                                    <td class="col-md-1">{{ $image->id }}</td>
                                    <td class="col-md-3"><img src="{{ Storage::url($image->path) }}" width="100%"></td>
                                    <td class="col-md-7">
                                        <form class="update_form" action="{{ route('images.update', ['id' => $image->id]) }}" method="POST" onsubmit="event.preventDefault()">
                                            {{ csrf_field() }}
                                            {{ method_field('POST') }}

                                            <input type="text" name="name" value="{{ $image->name }}" disabled style="width:100%;">
                                        </form>
                                    </td>
                                    <td class="col-md-1">
                                        <i class="edit glyphicon glyphicon-pencil"></i>
                                        <form class="destroy_form" action="{{ route('images.destroy', ['id' => $image->id]) }}" method="POST" onsubmit="event.preventDefault()">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                        </form>
                                        <i class="destroy glyphicon glyphicon-trash"></i>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script>
        $( document ).ready(function() {

            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

            $("#upload").fileinput({
                showUpload: false,
            });

            $("tbody").on("click", ".edit", function () {
                var form = $(this).parent().parent().find(".update_form");
                var click = $(this);
                console.log($(this).parent().parent().find(".update_form").serialize());
                if ($(this).hasClass("glyphicon glyphicon-ok")) {
                    $.ajax({
                        type: 'POST',
                        url: form.attr('action'),
                        data: form.serialize(),
                        success: function(data) {
                            click.removeClass("glyphicon glyphicon-ok").addClass("glyphicon glyphicon-pencil");
                            click.parent().parent().find("input").prop("disabled", true);
                            click.parent().parent().find("input[name='name']").prop("value", data);
                        }
                    });
                } else {
                    $(this).removeClass("glyphicon glyphicon-pencil").addClass("glyphicon glyphicon-ok");
                    $(this).parent().parent().find("input").prop("disabled", false);
                }
            });

            $("tbody").on("click", ".destroy", function () {
                var form = $(this).parent().find(".destroy_form");
                var click = $(this);
                $.ajax({
                    type: form.attr('method'),
                    url: form.attr('action'),
                    data: {_token: CSRF_TOKEN},
                    success: function(data) {
                        click.parent().parent().remove();
                    }
                });
            });

            $("#upload_form").submit(function () {
                var form = $(this);
                $.ajax({
                    type: 'POST',
                    url: form.attr('action'),
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data) {
                        $('tbody').append(data);
                    }
                });
            });

        });
    </script>
@endsection

