<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Route::group();

Route::get('images', ['as' => 'images.index', 'uses' => 'ImageController@index']);
Route::post('upload', ['as' => 'images.store', 'uses' => 'ImageController@store']);
Route::post('update/{id}', ['as' => 'images.update', 'uses' => 'ImageController@update']);
Route::post('destroy/{id}', ['as' => 'images.destroy', 'uses' => 'ImageController@destroy']);
