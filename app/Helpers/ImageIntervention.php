<?php

namespace App\Helpers;

use App\Helpers\Contracts\ImageContract;
use File;
use Intervention\Image\ImageManagerStatic as Image;

class ImageIntervention implements ImageContract {

    public function save($image, $path)
    {
        if (!File::exists(storage_path("app/public/" . $path))) {
            File::makeDirectory(storage_path("app/public/" . $path), 777, true);
        }
        $name = $image->getClientOriginalName();
        Image::make($image)->save(storage_path("app/public/" . $path . '/' . $name));

        return $name;
    }

}