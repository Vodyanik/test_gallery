<?php

namespace App\Helpers\Contracts;

interface ImageContract {

    public function save($image, $path);

}