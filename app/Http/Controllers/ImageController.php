<?php

namespace App\Http\Controllers;

use App\Helpers\Contracts\ImageContract;
use App\Image;
use Illuminate\Http\Request;
use Auth;

class ImageController extends Controller
{
    protected $defaultPath = 'images/';

    public function index()
    {
        $images = Image::all();
        return view('index', compact('images'));
    }

    public function store(Request $request, ImageContract $image)
    {
        $userId = Auth::user()->id;
        $path = $this->defaultPath . $userId . "/";

        $images = collect();
        foreach($request->file()['images'] as $img) {
            $newImage = new Image();
            $imageName = $image->save($img, $path);
            if (!empty($imageName)) {
                $newImage->user_id = $userId;
                $newImage->name = $imageName;
                $newImage->path = $path . $imageName;
                $newImage->save();
                $images->push($newImage);
            } else {
                return redirect()->back()->with('error', 'Ошибка! Не удалось сохранить!');
            }
        }

        return response()->view('image', compact('images'));
    }

    public function update(Request $request, $id)
    {
        $image = Image::find($id);
        $image->name = $request->name;
        $image->save();

        return $image->name;
    }

    public function destroy($id)
    {
        Image::find($id)->delete();
    }

}
